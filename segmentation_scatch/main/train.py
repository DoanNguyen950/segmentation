###             Basic libaries
import os 
import sys 
from pathlib import Path
import numpy as np
###             Frame work
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.image import load_img

###             Files
sys.path.append('/content/drive/My Drive/Backup/Segmentation/src/segmentation/segmentation_scatch')
import configs
from datasets import data
from models import unet

###         Metrics
def f1_score(y_true, y_pred):
    # print("Y_true: {}".format(dtype(y_true)))
    # print("Y_true: {}".format(type(y_pred)))
    tf.dtypes.cast(y_true, tf.float32)
    intersection = tf.math.reduce_sum( y_true * y_pred, axis=-1)
    denominator = tf.math.reduce_sum(y_true + y_pred, axis=-1)
    return (2*intersection) / denominator


def train():
    ###   free ram in case the model definition cells were run multiple times
    keras.backend.clear_session()
    ###         Data sequences
    input_train_paths, target_train_paths = data.sort_files(input_dir=configs.INPUT_TRAIN_DIR, target_dir=configs.TARGET_TRAIN_DIR)
    input_valid_paths, target_valid_paths = data.sort_files(input_dir=configs.INPUT_VALID_DIR, target_dir=configs.TARGET_VALID_DIR)


    train_gen = data.DestistrySegmentation(
        batch_size=configs.BATCH_SIZE,
        img_size=configs.IMAGE_SIZE,
        input_img_paths=input_train_paths,
        target_img_paths=target_train_paths
    )
    valid_gen = data.DestistrySegmentation(
        batch_size=configs.BATCH_SIZE,
        img_size=configs.IMAGE_SIZE,
        input_img_paths=input_valid_paths,
        target_img_paths=target_valid_paths
    )

    ###     build model
    model = unet.unet_model(configs.IMAGE_SIZE, configs.NUMB_CLASSES)
    model.compile(optimizer = Adam(lr = 1e-4), loss="binary_crossentropy", metrics = ['accuracy', f1_score])

    callbacks = [
        keras.callbacks.ModelCheckpoint("oxford_segmentation.h5", save_best_only=True)
    ]

    ###      Train the model, doing validation at the end of each epoch.
    epochs = 15
    model.fit(train_gen, epochs=configs.EPOCHS, validation_data=valid_gen, callbacks=callbacks)


def train():
    ###   free ram in case the model definition cells were run multiple times
    keras.backend.clear_session()
    ###         Data sequences
    input_train_paths, target_train_paths = data.sort_files(input_dir=configs.INPUT_TRAIN_DIR, target_dir=configs.TARGET_TRAIN_DIR)
    input_valid_paths, target_valid_paths = data.sort_files(input_dir=configs.INPUT_VALID_DIR, target_dir=configs.TARGET_VALID_DIR)


    train_gen = data.DestistrySegmentation(
        batch_size=configs.BATCH_SIZE,
        img_size=configs.IMAGE_SIZE,
        input_img_paths=input_train_paths,
        target_img_paths=target_train_paths
    )
    valid_gen = data.DestistrySegmentation(
        batch_size=configs.BATCH_SIZE,
        img_size=configs.IMAGE_SIZE,
        input_img_paths=input_valid_paths,
        target_img_paths=target_valid_paths
    )

    ###     build model
    model = unet.unet_model(configs.IMAGE_SIZE, configs.NUMB_CLASSES)
    model.compile(optimizer = Adam(lr = 1e-4), loss="binary_crossentropy", metrics = ['accuracy', f1_score])

    callbacks = [
        keras.callbacks.ModelCheckpoint("oxford_segmentation.h5", save_best_only=True)
    ]

    ###      Train the model, doing validation at the end of each epoch.
    model.fit(train_gen, epochs=configs.EPOCHS, validation_data=valid_gen, callbacks=callbacks)


def trani2():
    ###   free ram in case the model definition cells were run multiple times
    keras.backend.clear_session()
    ###         Data sequences
    input_train_paths, target_train_paths = data.sort_files(input_dir=configs.INPUT_TRAIN_DIR, target_dir=configs.TARGET_TRAIN_DIR)
    input_valid_paths, target_valid_paths = data.sort_files(input_dir=configs.INPUT_VALID_DIR, target_dir=configs.TARGET_VALID_DIR)


    train_gen = data.DestistrySegmentation(
        batch_size=configs.BATCH_SIZE,
        img_size=configs.IMAGE_SIZE,
        input_img_paths=input_train_paths,
        target_img_paths=target_train_paths
    )
    valid_gen = data.DestistrySegmentation(
        batch_size=configs.BATCH_SIZE,
        img_size=configs.IMAGE_SIZE,
        input_img_paths=input_valid_paths,
        target_img_paths=target_valid_paths
    )

    ###     build model
    model = unet.unet()
    model.compile(optimizer = Adam(lr = 1e-4), loss = 'binary_crossentropy', metrics = [f1_score])
    callbacks = [
        keras.callbacks.ModelCheckpoint("gum_segmentation.h5", save_best_only=True)
    ]

    ###      Train the model, doing validation at the end of each epoch.
    model.fit(train_gen, epochs=configs.EPOCHS, validation_data=valid_gen, callbacks=callbacks)


if __name__ == "__main__":
    # train()
    trani2()