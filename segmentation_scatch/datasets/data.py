###             Basic libaries
import os 
import sys 
from pathlib import Path
import numpy as np
###             Frame work
from tensorflow import keras
from tensorflow.keras.preprocessing.image import load_img

###             Files
sys.path.append('/content/drive/My Drive/Backup/Segmentation/src/segmentation/segmentation_scatch')
import configs


def sort_files(input_dir=configs.INPUT_TRAIN_DIR, target_dir=configs.TARGET_TRAIN_DIR):
    """     this function sort the file's name. Note input image (*.jpg) & target image (*.png)
    Args:
        - input_dir (str): the input direction
        - target_dir (str): the output direction
    Return:
        input_img_paths, target_img_paths (list)
    """
    input_img_paths= sorted(
        [
            os.path.join(input_dir, fname)
            for fname in os.listdir(input_dir)
            if fname.endswith(".jpg")
        ]
    )
    target_img_paths = sorted(
        [
            os.path.join(input_dir, fname)
            for fname in os.listdir(input_dir)
            if fname.endswith(".png")
        ]
    )

    return input_img_paths, target_img_paths


class DestistrySegmentation(keras.utils.Sequence):
    def __init__(self, batch_size, img_size, input_img_paths, target_img_paths):
        self.batch_size = batch_size
        self.img_size = img_size
        self.input_img_paths = input_img_paths 
        self.target_img_paths = target_img_paths

    def __len__(self):
        return len(self.target_img_paths) // self.batch_size

    def __getitem__(self, idx):
        """
        Returns tuple (input, target) correspond to batch # idx
        """
        i = idx * self.batch_size 
        batch_input_img_paths = self.input_img_paths[i : i + self.batch_size]
        batch_target_img_paths = self.target_img_paths[i : i + self.batch_size]
        x = np.zeros((self.batch_size,) + self.img_size + (3,), dtype="float32")
        for j, path in enumerate(batch_input_img_paths):
            img = load_img(path, target_size=self.img_size)
            x[j] = img
        y = np.zeros((self.batch_size,) + self.img_size + (1,), dtype="uint8")
        for j, path in enumerate(batch_target_img_paths):
            img = load_img(path, target_size=self.img_size, color_mode="grayscale")
            y[j] = np.expand_dims(img, 2)
        return x, y