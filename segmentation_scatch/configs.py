###             Basic libaries
import os 
import sys 
from pathlib import Path
### 

###         Paths
INPUT_TRAIN_DIR = '/content/drive/My Drive/Backup/Segmentation/Datasets/train/JPEGImages_crop'
TARGET_TRAIN_DIR = '/content/drive/My Drive/Backup/Segmentation/Datasets/train/MaskImage_crop'
INPUT_VALID_DIR = '/content/drive/My Drive/Backup/Segmentation/Datasets/valid/JPEGImages_crop'
TARGET_VALID_DIR = '/content/drive/My Drive/Backup/Segmentation/Datasets/valid/MaskImage_crop'
###         Configs parameters
IMAGE_SIZE = (752, 2176)
NUMB_CLASSES = 2
BATCH_SIZE = 1
EPOCHS = 20