import os 
import sys 
from model import *
from data import *
### 
from keras import backend as K
import tensorflow as tf
##          Files
import loading_preprocessing, configs


def train_1():

    tf.keras.backend.clear_session()
    tf.random.set_seed(0)
    data_gen_args = dict(rotation_range=0.2,
                        width_shift_range=0.05,
                        height_shift_range=0.05,
                        shear_range=0.05,
                        zoom_range=0.05,
                        horizontal_flip=True,
                        fill_mode='nearest')
                        

    myGene = trainGenerator(1, 
                            '/media/doannn/Data1/Work/Projects/Teeth/Datasets/data_dataset_voc_bose/bone',
                            'imgs_results','masks_results',
                            data_gen_args,
                            save_to_dir = None)

    model = unet()
    model_checkpoint = ModelCheckpoint('bone_489_1920.hdf5', monitor='loss',verbose=1, save_best_only=True)
    model.fit_generator(myGene, steps_per_epoch=600, epochs=100, callbacks=[model_checkpoint])


def run_model():
    tf.keras.backend.clear_session()
    tf.random.set_seed(0)

    input_train_img_paths, target_train_img_paths = loading_preprocessing.get_input_target_path('/root/datasets/train')
    train_datasets = loading_preprocessing.Teeth(
        configs.BATCH_SIZE, 
        (configs.IMAGE_HEIGHT, configs.IMAGE_WIDTH), 
        input_train_img_paths, 
        target_train_img_paths
    )

    input_test_img_paths, target_test_img_paths = loading_preprocessing.get_input_target_path('/root/datasets/valid')
    test_datasets = loading_preprocessing.Teeth(
        configs.BATCH_SIZE, 
        (configs.IMAGE_HEIGHT, configs.IMAGE_WIDTH), 
        input_test_img_paths, 
        target_test_img_paths
    )
    # train_dataset, test_dataset = loading_preprocessing.load_datasets()

    model_checkpoint = ModelCheckpoint('gum_752_2176.hdf5', monitor='loss',verbose=1, save_best_only=True)


    model = unet()
    model_history = model.fit(
        train_datasets,
        epochs=configs.EPOCHS,
        steps_per_epoch=configs.STEPS_PER_EPOCH,
        validation_steps=configs.VALIDATION_STEPS,
        validation_data=test_datasets,
        callbacks=[model_checkpoint]
    )

    return model, model_history

if __name__ == "__main__":
    run_model()

