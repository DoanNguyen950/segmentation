import os 
import sys 
import tensorflow as tf 
import tensorflow_datasets as tfds
from tensorflow import keras
import numpy as np
from tensorflow.keras.preprocessing.image import load_img
##          files
import configs


def normalize(input_image, input_mask):
    input_image = tf.cast(input_image, tf.float32) / 255.0
    input_mask  = tf.squeeze(tf.one_hot(tf.cast(input_mask-1, tf.uint8), depth=configs.CLASSES))
    
    return input_image, input_mask

@tf.function
def load_image_train(datapoint):
    input_image = tf.image.resize(datapoint['JPEGImages_crop'], (configs.IMAGE_HEIGHT, configs.IMAGE_WIDTH))
    input_mask = tf.image.resize(datapoint['MaskImage_crop'], (configs.IMAGE_HEIGHT, configs.IMAGE_WIDTH))

    return normalize(input_image, input_mask)

def load_image_test(datapoint):
    input_image = tf.image.resize(datapoint['JPEGImages_crop'], (configs.IMAGE_HEIGHT, configs.IMAGE_WIDTH))
    input_mask = tf.image.resize(datapoint['MaskImage_crop'], (configs.IMAGE_HEIGHT, configs.IMAGE_WIDTH))

    return normalize(input_image, input_mask)


def load_datasets():
    dataset, info = tfds.load('/root/datasets/seg', with_info=True)
    print(dataset)

    train = dataset['train'].map(load_image_train, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    test = dataset['valid'].map(load_image_test)

    train_dataset = train.cache().shuffle(configs.BUFFER_SIZE).batch(configs.BATCH_SIZE).repeat()
    train_dataset = train_dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    test_dataset = test.batch(configs.BATCH_SIZE)

    return train_dataset, test_dataset

############


def get_input_target_path(dataset_path):
    input_dataset = os.path.join(dataset_path, 'JPEGImages_crop')
    target_dataset = os.path.join(dataset_path, 'MaskImage_crop')


    input_img_paths = sorted(
        [
            os.path.join(input_dataset, fname)
            for fname in os.listdir(input_dataset)
            if fname.endswith(".jpg")
        ]
    )
    target_img_paths = sorted(
        [
            os.path.join(target_dataset, fname)
            for fname in os.listdir(target_dataset)
            if fname.endswith(".png") and not fname.startswith(".")
        ]
    )

    return input_img_paths, target_img_paths



class Teeth(keras.utils.Sequence):
    """Helper to iterate over the data (as Numpy arrays)."""

    def __init__(self, batch_size, img_size, input_img_paths, target_img_paths):
        self.batch_size = batch_size
        self.img_size = img_size
        self.input_img_paths = input_img_paths
        self.target_img_paths = target_img_paths

    def __len__(self):
        return len(self.target_img_paths) // self.batch_size

    def __getitem__(self, idx):
        """Returns tuple (input, target) correspond to batch #idx."""
        i = idx * self.batch_size
        batch_input_img_paths = self.input_img_paths[i : i + self.batch_size]
        batch_target_img_paths = self.target_img_paths[i : i + self.batch_size]
        x = np.zeros((configs.BATCH_SIZE,) + self.img_size + (3,), dtype="float32")
        for j, path in enumerate(batch_input_img_paths):
            img = load_img(path, target_size=self.img_size)
            x[j] = img
        y = np.zeros((configs.BATCH_SIZE,) + self.img_size + (1,), dtype="uint8")
        for j, path in enumerate(batch_target_img_paths):
            img = load_img(path, target_size=self.img_size, color_mode="grayscale")
            y[j] = np.expand_dims(img, 2)
        return x, y