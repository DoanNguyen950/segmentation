import os 
import sys
from pathlib import Path 
import tensorflow as tf 
# import tensorflow_datasets as tfds

"""
+ the numb of samples processed for each epoch = batch_size * steps_per_epochs

"""
# dataset, info = tfds.load('/root/datasets/seg', with_info=True)
# TRAIN_LENGTH = info.splits['train'].num_examples

EPOCHS = 30
BATCH_SIZE = 1
BUFFER_SIZE = 1000
STEPS_PER_EPOCH = 64
VALIDATION_STEPS = 10

IMAGE_HEIGHT = 752
IMAGE_WIDTH  = 2176
CHANNELS     = 3
CLASSES      = 2

# TRAIN_INPUT_PATH = "/root/datasets/train/JPEGImages_crop"
# TRAIN_TARGET_PATH = "/root/datasets/train/MaskImage_crop"
# VALID_INPUT_PATH = "/root/datasets/valid/JPEGImages_crop"
# VALID_TARGET_PATH = "/root/datasets/valid/MaskImage_crop"
